use std::mem;
use num::Integer;

/// Hypothesis: a > 0,  b > 0
pub fn gcd<I: Integer>(mut a: I, mut b: I) -> I {
    if a < b {
        mem::swap(&mut a, &mut b);
    }

    loop {
        // a = bq + r
        let (_, r) = a.div_rem(&b);
        if r.is_zero() {
            return b;
        }
        a = b;
        b = r;
    }
}

/// Returns (u, v, gcd) with au + bv = gcd
/// Hypothesis: a > 0, b > 0
pub fn bezout<I: Integer + Clone>(a: I, b: I) -> (I, I, I) {
    if a < b {
        let (u, v, gcd) = bezout_internal(b, a);
        (v, u, gcd)
    } else {
        bezout_internal(a, b)
    }
}

/// Hypothesis: a > b > 0
fn bezout_internal<I: Integer + Clone>(mut a: I, mut b: I) -> (I, I ,I) {
    let mut prev_u = I::one();
    let mut prev_v = I::zero();
    let mut u = I::zero();
    let mut v = I::one();

    loop {
        // a = bq + r
        let (q, r) = a.div_rem(&b);
        if r.is_zero() {
            return (u, v, b);
        }
        a = b;
        b = r;

        // FIXME: less clones?
        let next_u = prev_u - q.clone()*u.clone();
        let next_v = prev_v - q*v.clone();
        prev_u = mem::replace(&mut u, next_u);
        prev_v = mem::replace(&mut v, next_v);
    }
}

/// Hypothesis: x >= 0, n > 0
pub fn inverse<I: Integer + Clone>(x: I, n: I) -> Option<I> {
    // x*inv = 1 [mod n]
    // <-> x*inv + k*n = 1
    if x.is_zero() {
        return None;
    }
    let (inv, _, gcd) = bezout(x, n);
    if gcd == I::one() {
        Some(inv)
    } else {
        None
    }
}

/// Hypothesis g, n, N >= 0 et entiers
pub fn power<I: Integer + Clone>(mut b: I, mut e: I, n: &I) -> I {
    let two = I::one() + I::one();
    let mut pow = I::one();
    //b = b % n ?

    while e > I::zero() {
        let (q, r) = e.div_rem(&two);
        if !r.is_zero() {
            pow = (pow.clone()*b.clone()).mod_floor(n);
        }
        b = (b.clone()*b.clone()).mod_floor(&n);
        e = q;
    }

    pow
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_gcd() {
        assert_eq!(gcd(714, 340), 34);
        assert_eq!(gcd(255, 141), 3);
    }

    #[test]
    fn test_bezout() {
        assert_eq!(bezout(714, 340), (1, -2, 34));
        assert_eq!(bezout(255, 141), (-21, 38, 3));
    }

    #[test]
    fn test_inverse() {
        assert_eq!(inverse(5, 10), None);
        assert_eq!(inverse(5, 12), Some(5));
    }

    #[test]
    fn test_power() {
        assert_eq!(power(4, 13, &497), 445);
        assert_eq!(power(3, 65, &1271), 987);
    }
}
