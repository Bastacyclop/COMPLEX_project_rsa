extern crate num;
extern crate rand;

pub mod modular;
pub mod prime;

use num::bigint::{BigUint, RandBigInt};
use num::pow::pow;
use rand::Rng;

pub fn generate_pkrsa<R: Rng>(rng: &mut R, t: usize) -> BigUint {
    let two = BigUint::from(2u8);
    let max = pow(two, t);
    let mut gen_prime = || {
        loop {
            let n = rng.gen_biguint_below(&max);
            if prime::fermat_test(&n) && prime::rabin_miller_test(&n) {
                return n;
            }
        }
    };
    let p = gen_prime();
    let q = gen_prime();
    p * q
}
