use num::{self, Integer, FromPrimitive, ToPrimitive, One, Zero};
use num::bigint::BigUint;
use rand::Rng;
use modular;

// in O(sqrt(n))
pub fn trial_division_test(n: &BigUint) -> bool {
    let sup = BigUint::from_f64(n.to_f64().expect("bigint -> f64").sqrt().floor()).unwrap();
    if n <= &BigUint::one() { return false; }
    for i in num::range_inclusive(BigUint::from(2u8), sup) {
        if n.is_multiple_of(&i) { return false; }
    }
    true
}

pub fn generate<F: FnMut(&[BigUint])>(max: &BigUint, mut f: F) -> Vec<BigUint> {
    if max <= &BigUint::one() { return Vec::new(); }
    let mut primes = vec![2u8.into()];

    fn is_prime(n: &BigUint, primes: &[BigUint]) -> bool {
        for p in primes {
            if n.is_multiple_of(p) {
                return false;
            }
        }
        true
    }

    for n in num::range_inclusive(BigUint::from(3u8), max.clone()) {
        if is_prime(&n, &primes) {
            primes.push(n.clone());
            f(&primes);
        }
    }
    primes
}

pub fn generate_carmichaels<F: FnMut(BigUint)>(max: &BigUint, mut f: F) {
    fn test(p: &BigUint, q: &BigUint, r: &BigUint, c: &BigUint) -> bool {
        let one = &BigUint::one();
        let x = c - one;
        x.is_multiple_of(&(p - one)) &&
            x.is_multiple_of(&(q - one)) &&
            x.is_multiple_of(&(r - one))
    }

    generate(&max.div_floor(&BigUint::from(2u8 * 3)), |primes| {
        if primes.len() <= 3 {
            // no carmichael here
        } else {
            let (p, others) = primes.split_last().unwrap();
            for q in 0..others.len() {
                let x = p * &others[q];
                if &x > max { break; }
                for r in (q + 1)..others.len() {
                    let c = &x * &others[r];
                    if &c > max { break; }
                    if test(p, &others[q], &others[r], &c) {
                        f(c);
                    }
                }
            }
        }
    });
}

pub fn random_carmichael<R: Rng>(rng: &mut R, max: &BigUint) -> BigUint {
    let mut carmichaels = Vec::new();
    generate_carmichaels(max, |c| carmichaels.push(c));
    let i = rng.gen_range(0, carmichaels.len());
    carmichaels.swap_remove(i)
}

pub fn fermat_test(n: &BigUint) -> bool {
    let one = &BigUint::one();
    let two = &(one + one);
    if n < two { return false; }

    // TODO: randoum or not randoum ?
    let witnesses = [2u16, 3, 5, 7];
    for &a in &witnesses {
        let a = BigUint::from(a);
        if &modular::gcd(a.clone(), n.clone()) > one {
            return false;
        }
        if &modular::power(a, n - one, n) != one {
            return false;
        }
    }
    true
}

pub fn rabin_miller_test(n: &BigUint) -> bool {
    let one = &BigUint::one();
    let two = &(one + one);
    if n < two { return false; }
    if n == two { return true; }
    if n.is_multiple_of(two) { return false; }

    let neg_one = n - one;
    let mut s = BigUint::zero();
    let mut r = neg_one.clone();
    loop {
        let (div, rem) = r.div_rem(two);
        if &rem == one { break; }
        s = s + one;
        r = div;
    }

    // TODO: randoum or not randoum ?
    let witnesses = [2u16, 3, 5, 7];
    for &a in &witnesses {
        let a = BigUint::from(a);
        if &modular::gcd(a.clone(), n.clone()) > one {
            return false;
        }
        let mut rest = modular::power(a, r.clone(), n);
        if &rest == one || rest == neg_one { return true; }
        for _ in num::range(one.clone(), s.clone()) {
            rest = modular::power(rest, two.clone(), n);
            if rest == neg_one { return true; }
        }
    }

    false
}
