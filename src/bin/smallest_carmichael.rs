extern crate num;
extern crate project_rsa;

use num::BigUint;
use project_rsa::prime;

fn main() {
    print!("carmichaels <= 561:");
    prime::generate_carmichaels(&BigUint::from(561u16), |c|  {
        print!(" {}", c);
    });
    println!("");
}
