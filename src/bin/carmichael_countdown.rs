extern crate num;
extern crate time;
extern crate project_rsa;

use std::env;
use num::{Zero, BigUint};
use project_rsa::prime;

fn main() {
    let mut args = env::args();
    args.next();
    let timeout = args.next().map(|a| a.parse().expect("expected seconds"))
        .unwrap_or(5. * 60.);

    let marker = time::precise_time_s();
    let mut biggest = BigUint::zero();
    prime::generate_carmichaels(&BigUint::from(u64::max_value()), |c| {
        if c > biggest {
            biggest = c;
            println!("biggest: {}", biggest);
        }

        let time = time::precise_time_s() - marker;
        if time > timeout {
            panic!("countdowned");
        }
    });
}
