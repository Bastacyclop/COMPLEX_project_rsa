extern crate num;
extern crate project_rsa;

use std::env;
use num::BigUint;
use project_rsa::prime;

fn main() {
    let mut args = env::args();
    args.next();
    let n = args.next().expect("expected integer")
        .parse().expect("expected integer");

    let print_result = |name, b| {
        println!("{} test {}", name, if b { "passed" } else { "failed" });
    };
    print_result("fermat", prime::fermat_test(&n));
    print_result("rabin-miller", prime::rabin_miller_test(&n));
}
