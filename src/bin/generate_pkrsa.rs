extern crate rand;
extern crate project_rsa;

use std::env;

fn main() {
    let mut args = env::args();
    args.next();
    let t = args.next().map(|a| a.parse().expect("expected integer"))
        .unwrap_or(32usize);

    let rng = &mut rand::thread_rng();
    println!("generated {}", project_rsa::generate_pkrsa(rng, t));
}
