extern crate num;
extern crate project_rsa;

use std::env;
use num::BigUint;
use project_rsa::prime;

fn main() {
    let mut args = env::args();
    args.next();
    let max = args.next().map(|a| a.parse().expect("expected integer"))
        .unwrap_or_else(|| BigUint::from(100_000u32));
    let mut cs = Vec::new();
    prime::generate_carmichaels(&max, |c| cs.push(c));
    let count = cs.len();
    let mut cs = cs.iter();
    if let Some(c) = cs.next() {
        print!("{}", c);
        for c in cs { print!(", {}", c); }
    }
    println!("");
    println!("generated {} carmichaels", count);
}
