extern crate project_rsa;
extern crate time;
extern crate num;
extern crate rand;

use std::{fs, io, mem};
use std::io::prelude::*;
use num::{Integer, One, Zero};
use num::bigint::{BigUint, RandBigInt};
use rand::Rng;

use project_rsa::{modular, prime};

fn measure<F: FnOnce()>(f: F) -> u64 {
    let marker = time::precise_time_ns();
    f();
    time::precise_time_ns() - marker
}

fn main() {
    let mut buf = io::BufWriter::new(
        fs::File::create("benchmarks/pgcd").unwrap());
    let mut rng = &mut rand::thread_rng();

    let samples = 1_000;
    let max = u32::max_value();
    for _ in 0..samples {
        let a: u32 = rng.gen_range(1, max);
        let b = rng.gen_range(1, a);
        writeln!(buf, "{} {}", a, measure(|| { modular::gcd(a, b); }))
            .unwrap();
    }

    buf = io::BufWriter::new(
        fs::File::create("benchmarks/inverse").unwrap());

    for _ in 0..samples {
        let n: u32 = rng.gen_range(1, max);
        let x = rng.gen_range(0, n);
        writeln!(buf, "{} {}", n, measure(|| { modular::inverse(x, n); }))
            .unwrap();
    }

    let max = &BigUint::from(u16::max_value());
    let primes = &prime::generate(max, |_| {})[..];
    // no need to primes.sort();

    println!("-- fermat --");
    analyse_primality_test(rng, max, primes, prime::fermat_test);
    println!("-- rabin miller --");
    analyse_primality_test(rng, max, primes, prime::rabin_miller_test);
}

struct ErrorRatio {
    tested: f32,
    errors: f32,
}

impl ErrorRatio {
    fn new() -> ErrorRatio {
        ErrorRatio {
            tested: 0.,
            errors: 0.,
        }
    }

    fn add(&mut self, is_wrong: bool) {
        self.tested += 1.;
        if is_wrong { self.errors += 1.; }
    }

    fn dump(&mut self, name: &str) {
        println!("{}: {} error/test", name, self.errors / self.tested);
        mem::replace(self, ErrorRatio::new());
    }
}

fn analyse_primality_test<R, F>(rng: &mut R, max: &BigUint, primes: &[BigUint], test: F)
    where R: Rng, F: Fn(&BigUint) -> bool
{
    let is_wrong = |n: &BigUint| test(n) && primes.binary_search(n).is_err();

    let mut ratio = ErrorRatio::new();
    prime::generate_carmichaels(&max, |c| {
        ratio.add(is_wrong(&c));
    });
    ratio.dump("carmichael");

    let samples = 2_000;
    for _ in 0..samples {
        let a = rng.gen_biguint_range(&BigUint::one(), &max.div_floor(&BigUint::from(2u8)));
        let b = rng.gen_biguint_below(&max.div_floor(&a));
        ratio.add(is_wrong(&(a * b)));
    }
    ratio.dump("composed");

    for _ in 0..samples {
        let n = rng.gen_biguint_below(max);
        ratio.add(is_wrong(&n));
    }
    ratio.dump("random");

    for n in num::range(BigUint::zero(), BigUint::from(100_000u32)) {
        ratio.add(is_wrong(&n));
    }
    ratio.dump("<= 10^5");
}
