extern crate num;
extern crate project_rsa;

use std::env;
use num::BigUint;
use project_rsa::prime;

fn main() {
    let mut args = env::args();
    args.next();
    let max = args.next().map(|a| a.parse().expect("expected integer"))
        .unwrap_or_else(|| BigUint::from(100_000u32));
    let ps = prime::generate(&max, |_| {});
    let count = ps.len();
    let mut ps = ps.iter();
    if let Some(p) = ps.next() {
        print!("{}", p);
        for p in ps { print!(", {}", p); }
    }
    println!("");
    println!("generated {} primes", count);
}
